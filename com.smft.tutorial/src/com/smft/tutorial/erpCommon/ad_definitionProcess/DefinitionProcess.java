package com.smft.tutorial.erpCommon.ad_definitionProcess;

import java.util.Date;

import org.openbravo.base.provider.OBProvider;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.service.db.DalBaseProcess;

import com.smft.tutorial.SMFTEdicion;
import com.smft.tutorial.SMFTInscripcion;

public class DefinitionProcess extends DalBaseProcess {

  @Override
  public void doExecute(ProcessBundle bundle) throws Exception {
    try {
      Date today = new Date();
      final String alumnoId = (String) bundle.getParams().get("alumnoId");
      final String edicionId = (String) bundle.getParams().get("edicionId");

      SMFTInscripcion newInscripcion = OBProvider.getInstance().get(SMFTInscripcion.class);
      User actualUser = OBContext.getOBContext().getUser();
      Client currentClient = OBContext.getOBContext().getCurrentClient();
      Organization currentOrg = OBContext.getOBContext().getCurrentOrganization();

      newInscripcion.setNewOBObject(true);
      newInscripcion.setClient(currentClient);
      newInscripcion.setOrganization(currentOrg);
      newInscripcion.setActive(true);
      newInscripcion.setCreationDate(today);
      newInscripcion.setCreatedBy(actualUser);
      newInscripcion.setUpdated(today);
      newInscripcion.setUpdatedBy(actualUser);
      newInscripcion.setBpartner(OBDal.getInstance().get(BusinessPartner.class, alumnoId));
      newInscripcion.setSmftEdicion(OBDal.getInstance().get(SMFTEdicion.class, edicionId));

      OBDal.getInstance().save(newInscripcion);

      final StringBuilder sb = new StringBuilder();
      sb.append("El alumno se inscribió correctamente <br/>");

      OBError msg = new OBError();
      msg.setType("Success");
      msg.setMessage(sb.toString());
      msg.setTitle("Success");
      bundle.setResult(msg);
    } catch (final Exception e) {
      e.printStackTrace(System.err);
      final OBError msg = new OBError();
      msg.setType("Error");
      msg.setMessage(e.getMessage());
      msg.setTitle("Error occurred");
      bundle.setResult(msg);
    }
  }
}
