//PROPERTY_SMFTFECHADESDE

package com.smft.tutorial.erpCommon.ad_eventhandler;

import java.util.List;

import javax.enterprise.event.Observes;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.model.Entity;
import org.openbravo.base.model.ModelProvider;
import org.openbravo.base.model.Property;
import org.openbravo.client.kernel.event.EntityNewEvent;
import org.openbravo.client.kernel.event.EntityPersistenceEventObserver;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.model.common.plm.Product;
import org.openbravo.service.db.DalConnectionProvider;

import com.smft.tutorial.SMFTAsignatura;
import com.smft.tutorial.SMFTEdicion;
import com.smft.tutorial.SMFTInscripcion;
import com.smft.tutorial.SMFTTipo;

class TutorialEventHandler extends EntityPersistenceEventObserver {
  private static Entity[] entities = {
      ModelProvider.getInstance().getEntity(SMFTInscripcion.ENTITY_NAME) };
  private static final Logger logger = LogManager.getLogger();

  @Override
  protected Entity[] getObservedEntities() {
    return entities;
  }

  public void onSave(@Observes EntityNewEvent event) {
    if (!isValidEvent(event)) {
      return;
    }

    final SMFTInscripcion alumno = (SMFTInscripcion) event.getTargetInstance();
    final Entity alumnoEntity = ModelProvider.getInstance().getEntity(SMFTInscripcion.ENTITY_NAME);

    final Property propertyFechadesde = alumnoEntity
        .getProperty(SMFTInscripcion.PROPERTY_FECHADESDE);
    final Property propertyFechahasta = alumnoEntity
        .getProperty(SMFTInscripcion.PROPERTY_FECHAHASTA);
    final SMFTEdicion currentEdicion = alumno.getSmftEdicion();
    final String fechadesde = currentEdicion.getFecha() + "/01/01";

    final OBCriteria obc = OBDal.getInstance().createCriteria(SMFTAsignatura.class);
    obc.add(Restrictions.eq(SMFTAsignatura.PROPERTY_SMFTEDICION, currentEdicion));

    final OBCriteria curso = OBDal.getInstance().createCriteria(Product.class);
    curso.add(Restrictions.eq(Product.PROPERTY_ID, currentEdicion.getProduct().getId()));

    final List<Product> cursos = curso.list();
    final List<SMFTAsignatura> asignaturas = obc.list();

    int totalAnual = 0;
    int totalPrimer = 0;
    int totalSegundo = 0;
    for (SMFTAsignatura asign : asignaturas) {
      SMFTTipo currentTipo = asign.getSmftTipodictado();
      OBCriteria allTipos = OBDal.getInstance().createCriteria(SMFTTipo.class);
      allTipos.add(Restrictions.eq(SMFTTipo.PROPERTY_TIPO, "Anual"));
      List<SMFTTipo> dictados = allTipos.list();
      SMFTTipo selected = dictados.get(0);

      if (selected == currentTipo) {
        totalAnual++;
      } else {
        allTipos = OBDal.getInstance().createCriteria(SMFTTipo.class);
        allTipos.add(Restrictions.eq(SMFTTipo.PROPERTY_TIPO, "1er Cuatrimestre"));
        dictados = allTipos.list();
        selected = dictados.get(0);
        if (selected == currentTipo) {
          totalPrimer++;
        } else {
          totalSegundo++;
        }
      }
    }

    String fechahasta = "";
    int primerAño = Integer.parseInt(currentEdicion.getFecha());
    int ultimoAño = primerAño + totalAnual - 1;
    int primerosSegundos = Math.abs(totalPrimer - totalSegundo);
    ultimoAño += primerosSegundos;
    if (totalPrimer > totalSegundo) {
      ultimoAño += totalPrimer - primerosSegundos;
      fechahasta = ultimoAño + "/06/30";
    } else {
      ultimoAño += totalSegundo - primerosSegundos;
      fechahasta = ultimoAño + "/12/31";
    }

    Product currentCurso = cursos.get(0);
    final OBCriteria allEdiciones = OBDal.getInstance().createCriteria(SMFTEdicion.class);
    allEdiciones.add(Restrictions.eq(SMFTEdicion.PROPERTY_PRODUCT, currentCurso));
    List<SMFTEdicion> ediciones = allEdiciones.list();
    for (SMFTEdicion edicion : ediciones) {
      final OBCriteria allInscripciones = OBDal.getInstance().createCriteria(SMFTInscripcion.class);
      allInscripciones.add(Restrictions.eq(SMFTInscripcion.PROPERTY_SMFTEDICION, edicion));
      List<SMFTInscripcion> inscriptos = allInscripciones.list();
      for (SMFTInscripcion inscripto : inscriptos) {
        if (inscripto.getBpartner() == alumno.getBpartner()) {
          if (!menorFecha(inscripto.getFechahasta(), fechadesde)) {
            ConnectionProvider conn = new DalConnectionProvider(false);
            throw new OBException(Utility.messageBD(conn, "El alumno ya se inscribió al curso",
                "El alumno ya se inscribió al curso"));
          }
        }
      }
    }

    event.setCurrentState(propertyFechahasta, fechahasta);
    event.setCurrentState(propertyFechadesde, fechadesde);
  }

  private boolean menorFecha(String fecha1, String fecha2) {
    if (Integer.parseInt(fecha1.substring(0, 4)) <= Integer.parseInt(fecha2.substring(0, 4))) {
      if (Integer.parseInt(fecha1.substring(5, 7)) <= Integer.parseInt(fecha2.substring(5, 7))) {
        if (Integer.parseInt(fecha1.substring(8, 10)) < Integer.parseInt(fecha2.substring(8, 10))) {
          return true;
        }
      }
    }
    return false;
  }
}
