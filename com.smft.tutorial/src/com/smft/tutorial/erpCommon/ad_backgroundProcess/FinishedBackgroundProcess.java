package com.smft.tutorial.erpCommon.ad_backgroundProcess;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.scheduling.ProcessLogger;
import org.openbravo.service.db.DalBaseProcess;
import org.quartz.JobExecutionException;

import com.smft.tutorial.SMFTInscripcion;

public class FinishedBackgroundProcess extends DalBaseProcess {

  private ProcessLogger logger;

  @Override
  public void doExecute(ProcessBundle bundle) throws Exception {

    logger = bundle.getLogger();

    try {
      final Calendar cal = Calendar.getInstance();
      cal.add(Calendar.DATE, -1);
      DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
      String ayer = dateFormat.format(cal.getTime());
      final OBCriteria<SMFTInscripcion> vencidas = OBDal.getInstance()
          .createCriteria(SMFTInscripcion.class);
      vencidas.add(Restrictions.eq(SMFTInscripcion.PROPERTY_FECHAHASTA, ayer));
      final List<SMFTInscripcion> todas = vencidas.list();
      if (todas.size() != 0) {
        for (SMFTInscripcion vencida : todas) {
          String nombre = vencida.getBpartner().getName();
          logger.log("Inscripcion del alumno: " + nombre + "Vencida el día de ayer" + "\n");
        }
      } else {
        logger.log("No se venció ninguna edición");
      }
    } catch (Exception e) {
      throw new JobExecutionException(e.getMessage(), e);
    }
  }
}
